import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFluentModule, FluentPipe } from 'angular-fluent'
import {ChangeLocaleButtonComponent} from "./component/change-locale-button/change-locale-button.component";

@NgModule({
  declarations: [
    AppComponent,
    ChangeLocaleButtonComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFluentModule,
  ],
  providers: [
    FluentPipe,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
