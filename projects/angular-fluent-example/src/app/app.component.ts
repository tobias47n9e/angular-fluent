import {Component, OnInit} from '@angular/core';
import {AngularFluentService} from "angular-fluent";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  activeLocale: string | null = null;
  dogTranslation: string | null = null;
  catTranslation: string | null = null;
  bearTranslation: string | null = null;
  returnMessageKey: boolean = false;

  constructor(
    private angularFluentService: AngularFluentService,
  ) {
  }

  ngOnInit(): void {
    this.angularFluentService.setLocale("en");
    //this.angularFluentService.setFallbackLocales(["fr", "en"]);

    this.angularFluentService.locale.subscribe(locale => {
      this.activeLocale = locale;
      this.updateTranslations();
    });
  }

  changeLocale(event: string) {
    this.angularFluentService.setLocale(event);
    this.updateTranslations();
  }

  private updateTranslations() {
    this.angularFluentService.translate("dog").then(value => this.dogTranslation = value);
    this.angularFluentService.translate("cat").then(value => this.catTranslation = value);
    this.angularFluentService.translate("bear").then(value => this.bearTranslation = value);
  }

  returnKey(returnKey: boolean) {
    this.angularFluentService.setReturnKeyForUnresolvedMessages(returnKey);
    this.returnMessageKey = returnKey;
    this.updateTranslations();
  }
}
