import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-change-locale-button',
  templateUrl: './change-locale-button.component.html',
  styleUrls: ['./change-locale-button.component.scss']
})
export class ChangeLocaleButtonComponent implements OnInit {
  @Input()
  locale!: string;

  @Input()
  activeLocale: string | null = null;

  @Output()
  localeChanged: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  changeLocale(locale: string) {
   this.localeChanged.emit(locale);
  }

  get active(): boolean {
    return this.locale === this.activeLocale;
  }
}
