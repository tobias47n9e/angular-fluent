# Angular Fluent

An Angular package for translations using [Fluent](https://projectfluent.org/).

## Usage

### One locale

Put the translation files into the assets folder:

```
/assets/i18n/<locale>.ftl
```

Import the library in the app modules:

```typescript
import { AngularFluentModule } from "angular-fluent";

@NgModule({
  imports: [
    BrowserModule,
    AngularFluentModule,
    HttpClientModule,
  ],
  providers: [
    AngularFluentService,
  ],
})
```

In the app component import the main service and set the locale. For example
to initialize the service with German use:

```typescript
import { Component } from '@angular/core';
import { AngularFluentService } from 'angular-fluent';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent {
    constructor(private angularFluentService: AngularFluentService) {}

    ngOnInit() {
        this.angularFluentService.setLocale('de');
    }
}
```

#### Pipe

You can now use the pipe anywhere in your project to translate strings:

```html
{{ 'number-messages' | fluent:{number: 3} }}
```

To force a locale different from the one set in the service use:

```html
{{ 'number-messages' | fluent:{number: 3, locale: 'hu'} }}
```

#### Translation-Service

```typescript
export class AppComponent {
  async awaitTranslation(key: string) {
    const translation = await this.angularFluentService.translate(key);
    console.log(translation);
  }
}
```

### Language switching

Switching languges is as simple as calling the `setLocale()` function and
passing the new locale.

```typescript
export class AppComponent {
    switchLocale(locale: string) {
        this.angularFluentService.setLocale(locale);
    }
}
```

### Fallbacks and hybrid localization

Angular Fluent can store an array of fallback locales in the service, which
are used if a message can't be resolved in the primary locale. The
fallback locales are prioritized / used in the order of the given array.

This is particularly interesting if the locale priority is taken from
the user's browser locale settings. Because each message is resolved
individually that means a page can use hybrid localization, where if not
all locales are translated completely the messages are resolved
individually to provide the best translation according to a user's
language competences.

Fallback locales can be set like this and are then used automatically by
the translation pipe:

```typescript
export class AppComponent {
    setFallbackLocales() {
        const fallbacks: string[] = ['hu', 'es', 'pl'];
        this.angularFluentService.setFallbackLocales(fallbacks);
    }
}
```

### Return message key for unresolved messages

In some cases returning `null` for unresolved messages might be undesirable.
It is possible to return the message key instead:

```typescript
export class AppComponent {
    ngOnInit() {
        this.angularFluentService.setReturnKeyForUnresolvedMessages(true);
        this.angularFluentService.setLocale('de');
    }
}
```

## Installation

Install the following 2 packages:

```
npm i @fluent/bundle --save
npm i angular-fluent --save
```

## Development

### Build

Run `ng build angular-fluent` to build the project. The build artifacts will be stored in the `dist/` directory.

### Publishing

After building your library with `ng build angular-fluent`, go to the dist folder `cd dist/angular-fluent` and run `npm publish`.

### Running unit tests

Run `ng test angular-fluent` to execute the unit tests via [Karma](https://karma-runner.github.io).
