import {Pipe, PipeTransform} from "@angular/core";
import {AngularFluentService} from "./angular-fluent.service";
import {BehaviorSubject} from "rxjs";

function getLocale(args?: any): string | null {
  if (!args) {
    return null;
  }

  return args.locale ? args.locale : null;
}

@Pipe({
  name: "fluent",
  pure: false,
})
export class FluentPipe implements PipeTransform {
  localizationChanges: BehaviorSubject<null> | null = null;

  value: string | null = null;

  constructor(private fluentService: AngularFluentService) {
  }

  transform(key: string, args?: any): string | null {
    if (!this.localizationChanges) {
      this.localizationChanges = this.fluentService.localizationChanges;
    }

    this.localizationChanges.subscribe(() => {
      this.fluentService
        .translationObservable(key, args, getLocale(args))
        .subscribe(value => this.value = value);
    });

    return this.value;
  }
}
