import { NgModule } from '@angular/core';
import {FluentPipe} from "./fluent.pipe";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    FluentPipe,
  ],
  imports: [
    HttpClientModule,
  ],
  exports: [
    FluentPipe,
  ]
})
export class AngularFluentModule { }
