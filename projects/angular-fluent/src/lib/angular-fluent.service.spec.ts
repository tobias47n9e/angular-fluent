import {AngularFluentService} from './angular-fluent.service';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, of} from "rxjs";
import {LoadingState} from "./type/loading-state";

let httpClientSpy: jasmine.SpyObj<HttpClient>;
let service: AngularFluentService;

beforeEach(() => {
  httpClientSpy = jasmine.createSpyObj("HttpClient", ["get"]);
  service = new AngularFluentService(httpClientSpy);
});

it('should return locale', () => {
  expect(service.locale.value).toBe(null);
});

it('should return new locale', () => {
  service.setLocale("fr");
  expect(service.locale.value).toBe("fr");
});

it('subscription should return new locale', () => {
  service.setLocale("fr");

  service.locale.subscribe(locale => {
    expect(locale).toBe("fr");
  });
});

it('subscription should return null initially', () => {
  service.locale.subscribe(locale => {
    expect(locale).toBe(null);
  });
});

it('change locale subject should emit', () => {
  service.localizationChanges.subscribe(() => {
    expect().nothing();
  });

  service.setLocale("fr");
});

it('unresolved locale returns null', async () => {
  service.translationsMap.set("hu", null);
  service.translationLoadedEventMap.set("hu", {state: new BehaviorSubject<LoadingState>(LoadingState.NotFound)});

  const message = await service.translate("no-message", {}, "hu");

  expect(message).toBeNull();
});

it('unresolved locale returns key', async () => {
  service.translationsMap.set("hu", null);
  service.setReturnKeyForUnresolvedMessages(true);
  service.translationLoadedEventMap.set("hu", {state: new BehaviorSubject<LoadingState>(LoadingState.NotFound)});

  const message = await service.translate("no-message", {}, "hu");

  expect(message).toBe("no-message");
});

it('resolved locale and message returns translation', async () => {
  const data = `test-key = test-value`;
  httpClientSpy.get.and.returnValue(of(data));
  const message = await service.translate("test-key", {}, "de");

  expect(message).toBe("test-value");
});

it('resolved locale and unresolved message returns null', async () => {
  const data = `test-key = test-value`;
  httpClientSpy.get.and.returnValue(of(data));
  const message = await service.translate("key", {}, "de");

  expect(message).toBeNull();
});

it('resolved locale and unresolved message returns key', async () => {
  const data = `test-key = test-value`;
  httpClientSpy.get.and.returnValue(of(data));
  service.setReturnKeyForUnresolvedMessages(true);
  const message = await service.translate("key", {}, "de");

  expect(message).toBe("key");
});

it('two locales resolve 2 messages', async () => {
  const germanData = `dog = Hund`;
  httpClientSpy.get.and.returnValue(of(germanData));
  const germanMessage = await service.translate("dog", {}, "de");
  expect(germanMessage).toBe("Hund");

  const hungarianData = `dog = Kutya`;
  httpClientSpy.get.and.returnValue(of(hungarianData));
  const hungarianMessage = await service.translate("dog", {}, "hu");
  expect(hungarianMessage).toBe("Kutya");
});

it('two locales, but one message missing', async () => {
  const germanData = `dog = Hund`;
  httpClientSpy.get.and.returnValue(of(germanData));
  const germanMessage = await service.translate("dog", {}, "de");
  expect(germanMessage).toBe("Hund");

  const hungarianData = `dog = Kutya`;
  httpClientSpy.get.and.returnValue(of(hungarianData));
  const hungarianMessage = await service.translate("cat", {}, "hu");
  expect(hungarianMessage).toBeNull();
});
