import {FluentResource} from "@fluent/bundle";

export interface TranslationMap {
  [key: string]: FluentResource;
}
