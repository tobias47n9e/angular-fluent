export enum LoadingState {
  Loading = "loading",
  Done = "done",
  NotFound = "not_found",
}
