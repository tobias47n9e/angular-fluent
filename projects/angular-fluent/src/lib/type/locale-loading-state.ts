import {BehaviorSubject} from "rxjs";
import {LoadingState} from "./loading-state";

export interface LocaleLoadingState {
  state: BehaviorSubject<LoadingState>;
}
