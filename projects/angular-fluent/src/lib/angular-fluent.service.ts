import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {FluentBundle, FluentResource} from '@fluent/bundle';
import {LocaleLoadingState} from "./type/locale-loading-state";
import {LoadingState} from "./type/loading-state";

@Injectable({
  providedIn: 'root',
})
export class AngularFluentService {
  /**
   * Map of translation files
   *
   * Structure:
   *
   * {
   *   de: <content of de.ftl>,
   *   hu: <content of hu.ftl>
   * }
   */
  translationsMap: Map<string, FluentBundle | null> = new Map();

  /**
   * Map of each locale and its loading state
   */
  translationLoadedEventMap: Map<string, LocaleLoadingState> = new Map();

  /**
   * Default locale used if pipe is used without locale
   */
  locale: BehaviorSubject<string | null> = new BehaviorSubject<string | null>(null);

  /**
   * Fallback locales that are used in given order
   */
  fallBackLocales: BehaviorSubject<string[]> = new BehaviorSubject<string[]>(
    [],
  );

  /**
   * Emits an empty value whenever changes to localization resources happen
   */
  localizationChanges: BehaviorSubject<null> = new BehaviorSubject<null>(null);

  private returnKeyForUnresolved: boolean = false;

  constructor(private http: HttpClient) {
  }

  /**
   * Takes a key, options, an optional locale and returns an observable translation
   */
  translationObservable(key: string, options?: any, locale?: string | null): Observable<string | null> {
    const usedLocale = locale ? locale : this.locale.value;

    if (!usedLocale) {
      return of(this.returnKeyForUnresolved ? key : null);
    }

    if (!this.translationLoadedEventMap.has(usedLocale)) {
      this.fetchFile(usedLocale);
    }

    return this.resolveKey(key, options, usedLocale);
  }

  async translate(key: string, options?: any, locale?: string | null): Promise<string | null> {
    const usedLocale = locale ? locale : this.locale.value;

    if (!usedLocale) {
      return this.returnKeyForUnresolved ? key : null;
    }

    if (!this.translationLoadedEventMap.has(usedLocale)) {
      this.fetchFile(usedLocale);
    }

    const localeLoadingState = this.translationLoadedEventMap.get(usedLocale);

    if (localeLoadingState && localeLoadingState.state.value === LoadingState.Done) {
      return this.resolveKeyPromise(key, options, usedLocale);
    }

    if (localeLoadingState && localeLoadingState.state.value === LoadingState.Loading) {
      return new Promise<string | null>(resolve => {
        // This BehaviourSubject should return "loading" as the first state, which is
        // ignored. Only "done" or "not_found" will resolve the promise
        localeLoadingState.state.subscribe(async state => {
          if (state === LoadingState.NotFound) {
            return resolve(this.resolveFallbackPromise(key, options));
          }

          if (state === LoadingState.Done) {
            return resolve(this.resolveKeyPromise(key, options, usedLocale));
          }
        });
      });
    }

    return this.resolveKeyPromise(key, options, usedLocale);
  }

  setLocale(locale: string) {
    this.locale.next(locale);
    this.localizationChanges.next(null);
  }

  setFallbackLocales(locales: string[]) {
    this.fallBackLocales.next(locales);
    this.localizationChanges.next(null);
  }

  setReturnKeyForUnresolvedMessages(value: boolean) {
    this.returnKeyForUnresolved = value;
    this.localizationChanges.next(null);
  }

  get returnKeyForUnresolvedMessages() {
    return this.returnKeyForUnresolved;
  }

  /**
   * Resolves a message in the primary language first and then tries the fallback locales
   */
  private resolveKey(
    key: string,
    options: any,
    locale: string,
  ): Observable<string | null> {
    const bundle = this.translationsMap.get(locale);

    if (!bundle) {
      return of(null);
    }

    const message = bundle.getMessage(key);

    if (message && message.value) {
      return of(bundle.formatPattern(message.value, options));
    }

    return this.resolveWithFallBack(key, options);
  }

  /**
   * Fetches a locale file, writes it to the cache and then emits update
   */
  private fetchFile(locale: string) {
    this.translationLoadedEventMap.set(locale, {state: new BehaviorSubject<LoadingState>(LoadingState.Loading)});
    this.http
      .get(`assets/i18n/${locale}.ftl`, {
        responseType: 'text',
      }).subscribe({
      next: content => {
        const bundle = new FluentBundle(locale);
        const resource = new FluentResource(content);
        const errors = bundle.addResource(resource);

        if (errors.length) {
          console.error(errors);
        }

        this.translationsMap.set(locale, bundle);
        const eventEmitter = this.translationLoadedEventMap.get(locale);

        if (eventEmitter && eventEmitter.state) {
          eventEmitter.state.next(LoadingState.Done);
        }

        this.localizationChanges.next(null);
      },
      error: error => {
        if (locale === this.locale.value) {
          console.error(`Error loading ${locale}.ftl`);
          console.error(error);
        } else {
          console.warn(
            `Error loading fallback locale ${locale}.ftl`,
          );
          console.warn(error);
        }

        const eventEmitter = this.translationLoadedEventMap.get(locale);

        if (eventEmitter) {
          eventEmitter.state.next(LoadingState.NotFound);
        }

        this.localizationChanges.next(null);
      }
    });
  }

  /**
   * Iterates over the fallback locales and fetches the message in the first available locale
   */
  private resolveWithFallBack(key: string, options: any): Observable<string | null> {
    const fallbacks = this.fallBackLocales.value;

    for (const locale of fallbacks) {
      if (!this.translationLoadedEventMap.has(locale)) {
        this.fetchFile(locale);
      }

      const bundle = this.translationsMap.get(locale);

      if (!bundle) {
        return of(this.returnKeyForUnresolved ? key : null);
      }

      const message = bundle.getMessage(key);

      if (message && message.value) {
        return of(bundle.formatPattern(message.value, options));
      }
    }

    return of(this.returnKeyForUnresolved ? key : null);
  }

  private async resolveKeyPromise(key: string, options: any, locale: string): Promise<string | null> {
    const bundle = this.translationsMap.get(locale);

    if (!bundle) {
      return this.resolveFallbackPromise(key, options);
    }

    const message = bundle.getMessage(key);

    if (message && message.value) {
      return bundle.formatPattern(message.value, options);
    }

    return this.resolveFallbackPromise(key, options);
  }

  private async resolveFallbackPromise(key: string, options: any): Promise<string | null> {
    const fallbacks = this.fallBackLocales.value;

    if (!fallbacks.length) {
      return this.returnKeyForUnresolved ? key : null;
    }

    return new Promise<string | null>(async resolve => {
      for (const locale of fallbacks) {
        if (!this.translationLoadedEventMap.has(locale)) {
          this.fetchFile(locale);
        }

        const value = await this.resolveFallback(key, options, locale);

        if (value) {
          return resolve(value);
        }
      }

      return resolve(this.returnKeyForUnresolved ? key : null);
    });
  }

  private async resolveFallback(key: string, options: any, locale: string): Promise<string | null> {
    const eventEmitter = this.translationLoadedEventMap.get(locale);

    return new Promise<string | null>(resolve => {
      // If the loading subject exists, and the locale is currently loading, we wait for the result
      if (eventEmitter && eventEmitter.state.value === LoadingState.Done) {
        return resolve(new Promise(innerResolve => {
          eventEmitter.state.subscribe(() => {
            const message = this.resolveMessage(key, options, locale);
            return innerResolve(message);
          });
        }));
      }

      return resolve(this.resolveMessage(key, options, locale));
    });
  }

  private resolveMessage(key: string, options: any, locale: string): string | null {
    const bundle = this.translationsMap.get(locale);

    if (!bundle) {
      return this.returnKeyForUnresolved ? key : null;
    }

    const message = bundle.getMessage(key);

    if (message && message.value) {
      return bundle.formatPattern(message.value, options);
    }

    return this.returnKeyForUnresolved ? key : null;
  }
}
